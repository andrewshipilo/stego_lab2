import sys
import bitarray
import os
import random
import numpy as np
import math
from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot, QBuffer
from PyQt5.QtGui import *
from PIL import Image
from PIL import ImageQt


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'StegoLab2'
        self.fileName = 'None'
        self.verticalGroupBox = QGroupBox()
        self.capacityLabel = QLabel('')
        self.initUI()
        self.image = None

    def initUI(self):
        self.setWindowTitle(self.title)

        self.layout = QVBoxLayout()
        self.getLayoutButton = QPushButton('GET', self)
        self.getLayoutButton.clicked.connect(self.createGetLayout)
        self.layout.addWidget(self.getLayoutButton)

        self.insertLayoutButton = QPushButton("INSERT", self)
        self.insertLayoutButton.clicked.connect(self.createInsertLayout)
        self.layout.addWidget(self.insertLayoutButton)

        self.verticalGroupBox.setLayout(self.layout)

        self.windowLayout = QVBoxLayout()
        self.windowLayout.addWidget(self.verticalGroupBox)

        self.setLayout(self.windowLayout)
        self.show()

    @pyqtSlot()
    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fileName, _ = QFileDialog.getOpenFileName(self, "Choose file to open", "",
                                                       "Bmp Files (*.bmp);;All files (*.*)", options=options)
        if self.fileName:
            self.fileNameLabel.setText("File chosen: " + self.fileName)
            # Show image
            self.image = Image.open(self.fileName)
            # Convert to QImage
            qImage = ImageQt.ImageQt(self.image)
            self.image.show()
            #self.imageLabel.setPixmap(QPixmap.fromImage(qImage))

            # Calculate container capacity
            # TODO: implement


    @pyqtSlot()
    def insertPhrase(self):
        filename = os.path.splitext(os.path.basename(self.fileName))[0]
        extension = os.path.splitext(os.path.basename(self.fileName))[1]

        red_ch = self.image.split()[0]
        green_ch = self.image.split()[1]
        blue_ch = self.image.split()[2]

        x_size = self.image.size[0]
        y_size = self.image.size[1]

        ba = bitarray.bitarray()
        text = self.textbox.text()
        ba.frombytes(text.encode('utf-8') + b'0xE2')
        bits = ba.tolist()

        random.seed(self.keyTextbox.text())
        if len(bits) < x_size * y_size:
            rng = random.sample(range(0, x_size * y_size), len(bits))
        else:
            rng = random.sample(range(0, x_size * y_size), x_size * y_size)

        encoded_image = self.image.copy()
        pixels = encoded_image.load()

        cnt = 0
        bits_added = 0
        for num in rng:
            i, j = int(num % x_size), int(num / x_size)
            blue_ch_pix = bin(blue_ch.getpixel((i, j)))
            if bits[cnt] == 1:
                blue_ch_pix = blue_ch_pix[:-1] + '1'
                bits_added += 1
            else:
                blue_ch_pix = blue_ch_pix[:-1] + '0'
                bits_added += 1

            pixels[i, j] = (red_ch.getpixel((i, j)), green_ch.getpixel((i, j)), int(blue_ch_pix, 2))
            cnt += 1

        encoded_image.save(filename + '_res' + extension)

        print(str(len(bits)))
        print(str(bits_added))
        bits_left = len(bits) - bits_added

        if bits_left == 0:
            self.confirmLabel.setText('Phrase was fully inserted')
        else:
            bits_left = len(bits) - cnt
            self.confirmLabel.setText(
                'Phrase partially added, because container is too small, ' + str(bits_left) + ' bits of phrase left')

        print('Inserted')
        psnr = calculatePSNR(self.image, encoded_image)
        print('Psnr is', psnr)
    @pyqtSlot()
    def getPhrase(self):
        blue_ch = self.image.split()[2]

        x_size = self.image.size[0]
        y_size = self.image.size[1]

        ba = bitarray.bitarray()
        random.seed(self.keyTextbox.text())
        rng = random.sample(range(0, x_size * y_size), x_size * y_size) # We don't really know how long is message

        for num in rng:
            i, j = int(num % x_size), int(num / x_size)
            if bin(blue_ch.getpixel((i, j)))[-1] == '0':
                ba.append(0)
            else:
                ba.append(1)

        phrase = ba.tobytes().split(b'0xE2')
        try:
            self.phraseLabel.setText(phrase[0].decode("utf-8"))
        except:
            self.phraseLabel.setText('Key is wrong')

    @pyqtSlot()
    def createGetLayout(self):
        QWidget().setLayout(self.layout)
        self.layout = QVBoxLayout()

        label = QLabel("Enter key: ")
        self.layout.addWidget(label)
        self.keyTextbox = QLineEdit(self)
        self.layout.addWidget(self.keyTextbox)

        self.fileNameLabel = QLabel("File chosen: " + self.fileName)
        self.layout.addWidget(self.fileNameLabel)

        chooseButton = QPushButton('Choose file', self)
        chooseButton.setToolTip('Open file')
        chooseButton.clicked.connect(self.openFileNameDialog)
        self.layout.addWidget(chooseButton)

        self.layout.addWidget(self.capacityLabel)

        self.imageLabel = QLabel()
        self.layout.addWidget(self.imageLabel)

        getButton = QPushButton('Get', self)
        getButton.clicked.connect(self.getPhrase)
        self.layout.addWidget(getButton)

        self.phraseLabel = QLabel('')
        self.layout.addWidget(self.phraseLabel)

        self.verticalGroupBox.setLayout(self.layout)

    @pyqtSlot()
    def createInsertLayout(self):
        QWidget().setLayout(self.layout)
        self.layout = QVBoxLayout()

        label = QLabel("Enter key: ")
        self.layout.addWidget(label)

        self.keyTextbox = QLineEdit(self)
        self.layout.addWidget(self.keyTextbox)

        label1 = QLabel("Enter phrase to insert")
        self.layout.addWidget(label1)

        self.textbox = QLineEdit(self)
        self.layout.addWidget(self.textbox)

        self.fileNameLabel = QLabel("File chosen: " + self.fileName)
        self.layout.addWidget(self.fileNameLabel)

        chooseButton = QPushButton('Choose file', self)
        chooseButton.setToolTip('Open file')
        chooseButton.clicked.connect(self.openFileNameDialog)
        self.layout.addWidget(chooseButton)

        self.layout.addWidget(self.capacityLabel)

        self.imageLabel = QLabel()
        self.layout.addWidget(self.imageLabel)

        insertButton = QPushButton('INSERT', self)
        insertButton.clicked.connect(self.insertPhrase)
        self.layout.addWidget(insertButton)

        self.confirmLabel = QLabel('')
        self.layout.addWidget(self.confirmLabel)

        self.verticalGroupBox.setLayout(self.layout)


def calculatePSNR(src, chg):
    mse = np.mean((np.array(src) - np.array(chg)) ** 2)
    if mse == 0:
        return 100
    PIXEL_MAX = 255.0

    return 20 * math.log10(PIXEL_MAX / math.sqrt(mse))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
